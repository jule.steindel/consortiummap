/*
Copyright 2023 TU Dresden, NFDI4Earth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

var map;

// Executed once the web page is completely loaded
$(document).ready(function () {

    // Show main div if JavaScript enabled and available
    document.getElementById("divMain").style.display = "block";

    map = L.map("divMap").setView([51.16, 10.45], 6); // Geographical coordinates and zoom level of the map

    /*
    // Tile layer of the map using https://basemap.de/web-vektor/
    L.maplibreGL({
        style: 'https://sgx.geodatenzentrum.de/gdz_basemapde_vektor/styles/bm_web_col.json',
        attribution: 'Map data: &copy <a href="https://www.wikidata.org/wiki/Wikidata:Licensing">Wikidata</a>  &amp; <a href="https://ror.org/terms-of-use/">ROR</a> | Background tiles: &copy; <a href="https://basemap.de/web-vektor/">basemap.de</a> / BKG' // https://sgx.geodatenzentrum.de/web_public/gdz/lizenz/deu/basemapde_web_dienste_lizenz.pdf
    }).addTo(map);
    */

    // Tile layer of the map using https://gdz.bkg.bund.de/index.php/default/webdienste/topplus-produkte/wmts-topplusopen-wmts-topplus-open.html
    L.tileLayer("https://sgx.geodatenzentrum.de/wmts_topplus_open/tile/1.0.0/web_light/default/WEBMERCATOR/{z}/{y}/{x}.png", {
        format: "image/png",
        attribution: 'Map data: &copy <a href="https://www.wikidata.org/wiki/Wikidata:Licensing">Wikidata</a>  &amp; <a href="https://ror.org/terms-of-use/">ROR</a> | Background tiles: &copy  Bundesamt  f&uumlr  Kartographie  und  Geod&aumlsie (BKG),  <a  href= "https://sgx.geodatenzentrum.de/web_public/gdz/datenquellen/Datenquellen_TopPlusOpen.html">data sources</a>'
    }).addTo(map);

    // Reset map button
    $("#resetMapButton").on("click", function () {
        map.closePopup();
        map.setView([51.16, 10.45], 6);
    });

    // Markers
    var allMarkers = []; // Array for the markers

    // Define functions
    function onEachParticipant(participants, number, success, error) {
        currentParticipant = participants["@graph"][number];
        var identifier = currentParticipant["@id"].split("/").pop(); // Unique identifier for each participant

        // Homepage
        var homepage = 0; // Fallback

        if (typeof currentParticipant["homepage"] != "undefined") {
            var homepage = "<a href='" + currentParticipant["homepage"] + "' target='_blank'>Homepage</a>";

            if (currentParticipant["homepage"].length < 12) { // If there is more than one homepage, choose the first one
                var homepage = "<a href='" + currentParticipant["homepage"][0] + "' target='_blank'>Homepage</a>";
            };
        };

        // Location
        var lat = []; // Fallback
        var lng = []; // Fallback

        var geometryID = currentParticipant["hasGeometry"];

        if (typeof geometryID != "undefined") {

            if ($.isArray(geometryID)) { // Multiple geometries, saved in one array
                console.debug("Member has more than one geometry.")
                var lat = [];
                var lng = [];

                geometryID.forEach((geometry) => {
                    var geometryType = participants["@graph"].find(element => element["@id"] === geometry)["@type"];

                    if (geometryType == "sf:Point") {
                        var location = participants["@graph"].find(element => element["@id"] === geometry)["asWKT"];

                        // Save lng and lat in arrays
                        if (location.split(" ").length == 2) {
                            lng.push(location.split(" ")[0].split("(")[1]);
                            lat.push(location.split(" ")[1].split(")")[0]);
                        }

                        else {
                            lng.push(location.split(" ")[1].split("(")[1]);
                            lat.push(location.split(" ")[2].split(")")[0]);
                        };
                    }

                    else {
                        console.debug("Unsupported location type '" + geometryType + "'. Only sf:Point is supported.");
                    };
                })
            }

            else { // Only one geometry
                var lat = [];
                var lng = [];

                var geometryType = participants["@graph"].find(element => element["@id"] === geometryID)["@type"];

                if (geometryType == "sf:Point") {
                    var location = participants["@graph"].find(element => element["@id"] === geometryID)["asWKT"];

                    if (location.split(" ").length == 2) {
                        lng.push(location.split(" ")[0].split("(")[1]);
                        lat.push(location.split(" ")[1].split(")")[0]);
                    }

                    else {
                        lng.push(location.split(" ")[1].split("(")[1]);
                        lat.push(location.split(" ")[2].split(")")[0]);
                    };
                }

                else {
                    console.debug("Unsupported location type '" + geometryType + "'. Only sf:Point is supported.");
                };
            };

        };

        // Logo
        // Fallback
        var logo = "nologo.png";

        // If there is no logo from Wikidata in the KH or an exception (SVG from Wikidata not scaled correctly)
        // Exceptions: Alfred Wegener Institute for Polar and Marine Research, Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Jülich Research Centre, Leipzig University, Museum für Naturkunde - Leibniz Institute for Evolution and Biodiversity Science
        var sourceSystemID = currentParticipant["sourceSystemID"]; // ROR ID or Wikidata ID

        if (typeof currentParticipant["hasLogo"] == "undefined" || sourceSystemID == "032e6b942" || sourceSystemID == "04z8jg394" || sourceSystemID == "02nv7yv05" || sourceSystemID == "03s7gtk40" || sourceSystemID == "052d1a351") {
            var memberWithoutLogo = missingLogos.find(element => element["sourceSystemID"] == sourceSystemID);

            if (typeof memberWithoutLogo != "undefined") {
                var logoURL = memberWithoutLogo["logoURL"];
                var logo = logoURL;
            }
            else {
                console.debug("No logo available for %s", currentParticipant["name"][0]["@value"]);
            };
        }
        // Logo is available in the KH
        else {
            var logo = currentParticipant["hasLogo"];

            preload_image_object = new Image();
            preload_image_object.src = currentParticipant["hasLogo"];
        };

        // Name
        var nameEN = currentParticipant["name"].find(element => element["@language"] == "en");
        var nameDE = currentParticipant["name"].find(element => element["@language"] == "de");

        var name = currentParticipant["@id"]; // Use identifier as fallback name

        // Search for english name
        if (typeof nameEN != "undefined") {
            var name = nameEN["@value"];
        }

        // Take name without language tag if english name doesn't exist
        else if (currentParticipant["name"].find(element => typeof element.length !== "undefined")) {
            var name = currentParticipant["name"].find(element => typeof element.length !== "undefined");
        }

        else if (typeof nameDE != "undefined") {
            var name = nameDE["@value"];
        };

        // Contact person
        var contactPerson = 0; // Fallback
        var email = 0; // Fallback

        var urlContact = currentParticipant["hasNFDI4EarthContactPerson"];

        console.debug(urlContact);
        // Minimum length of URL to ensure that it starts with "https://nfdi4earth-knowledgehub.geo.tu-dresden.de/api/objects/n4ek"
        if (typeof urlContact != "undefined" && urlContact.length > 65) {

            ajax = $.ajax({

                dataType: "JSON",
                url: urlContact,
                success: function (contactInformation) {

                    contactPerson = "" + contactInformation["schema:name"] + "";
                    if (typeof contactPerson != "undefined") {
                        email = contactInformation["schema:email"];
                    }

                    createPopupAndList(name, homepage, logo, lat, lng, identifier, contactPerson, email, success, error);
                },

                error: function () {
                    console.debug("Getting contact information failed for " + name);
                    error({
                        error: "Getting contact information failed for " + name
                    });
                }

            });

        }

        else {
            createPopupAndList(name, homepage, logo, lat, lng, identifier, contactPerson, email, success, error);
        };
    };

    // Function to create popup und list element
    function createPopupAndList(name, homepage, logo, lat, lng, identifier, contactPerson, email, success, error) {

        emailPopup = "<a href='mailto:" + email + "'>Send email</a>";
        emailCard = "<a href='mailto:" + email + "'>" + email + "</a>";

        popupContent = "";
        cardContent = "";

        // HTML templates as functions
        function templatePopup(logo, name, homepage, contactPerson, emailPopup) {

            if (contactPerson != 0 && email != 0) { // If there is a contact person
                // Use divider for logo to make sure that the popup is centered over the marker
                return `
                <div class='logoDiv'><img src=${logo} class='logo'></img></div><br/>
                <span class='namePopup'>${name}</span><br/>
                <span class='homepagePopup'>${homepage}</span><br/><br/>
                <span class='contactNamePopup'>${contactPerson}</span><br/>
                <span class='emailPopup'>${emailPopup}</span>
                `
            }

            else {
                return `
                <div class='logoDiv'><img src=${logo} class='logo'></img></div><br/>
                <span class='namePopup'>${name}</span><br/>
                <span class='homepagePopup'>${homepage}</span>
                `
            };
        };

        function templateCard(identifier, logo, name, homepage, contactPerson, emailCard) {

            if (contactPerson != 0 && email != 0) { // If there is a contact person
                return `
                <div class='member' id=${identifier}>
                    <div class='logoDivGrid'><div class='logoDiv'><img src=${logo} class='logo'></img></div></div>
                        <hr class='hrGrid'></hr>
                        <div class='text'>
                            <b><span class ='nameCard'>${name}</span></b>
                            <br></br>
                            <br>${homepage}</br>
                            <br></br>
                            <span class='contactNameCard'>${contactPerson}</span>
                            <br><span class='emailCard'>${emailCard}</span></br>
                        </div>
                    </div>
                </div>
                `
            }

            else {
                return `
                <div class='member' id=${identifier}>
                    <div class='logoDivGrid'><div class='logoDiv'><img src=${logo} class='logo'></img></div></div>
                        <hr class='hrGrid'></hr>
                        <div class='text'>
                            <b><span class ='nameCard'>${name}</span></b>
                            <br></br>
                            <br>${homepage}</br>
                        </div>
                    </div>
                </div>
                `
            };
        };

        popupContent = templatePopup(logo, name, homepage, contactPerson, emailPopup);
        cardContent = templateCard(identifier, logo, name, homepage, contactPerson, emailCard);

        // $("#membersGrid").append(myTag`That ${person} is a ${age}.`);
        $("#membersGrid").append(cardContent);

        if (lat != [] && lng != []) {
            // Markers, each one has a unique ID (Everything after the last "/" of "@id"), and popups

            for (let i = 0; i < lat.length; i++) {
                var marker = L.marker([lat[i], lng[i]], {
                    id: identifier,
                    icon: createPersonalIcon(n4eBlue)
                }).addTo(map).bindPopup(popupContent);

                marker.on('click', function () {
                    var id = marker.options.id; // ID of clicked marker
                    var selected = [];

                    // Reset the colour of all the markers and get all markers with the same ID for members with multiple locations
                    $.each(allMarkers, (index, marker) => {
                        marker.setIcon(createPersonalIcon(n4eBlue));

                        if (marker.options.id === id) {
                            selected.push(marker);
                        };
                    });

                    for (let i = 0; i < selected.length; i++) {
                        selected[i].setIcon(createPersonalIcon(n4eLightBlue)); // Change colour to light blue for all the selected markers
                        selected[i].setZIndexOffset(1000); // Move selected markers to the front to make them visible
                    };
                });

                marker.getPopup().on('remove', function () {
                    var id = marker.options.id;
                    var selected = [];

                    // Reset the colour and the z-index of all the markers with the same ID
                    $.each(allMarkers, (index, marker) => {
                        if (marker.options.id === id) {
                            selected.push(marker);
                        };
                    });

                    for (let i = 0; i < selected.length; i++) {
                        selected[i].setIcon(createPersonalIcon(n4eBlue)); // Change colour to light blue if selected
                        selected[i].setZIndexOffset(0);
                    };
                });

                // Add to array
                allMarkers.push(marker);
            };

        };

        success({
            id: identifier,
            marker: marker
        });
    };

    // Function to open popup automatically by comparing title of the marker and the ID of clicked element
    function markerOpenPopup(id) {
        var selected = [];

        // Get all markers with the same ID for members with multiple locations
        $.each(allMarkers, (index, marker) => {
            if (marker && marker.options.id === id) {
                selected.push(marker);
            };
        });

        for (let i = 0; i < selected.length; i++) {
            selected[i].setIcon(createPersonalIcon(n4eLightBlue)); // Change colour to light blue if selected
            selected[i].setZIndexOffset(1000); // Move selected markers to the front to make them visible
        };

        selected[selected.length - 1].openPopup(); // Open popup of the last marker
        $(window).scrollTop($('#divMap').offset().top);
        return false;
    };

    const n4eBlue = "#05668D"; // Marker colours: NFDI4Earth blue with white border
    const n4eLightBlue = "#C0D6DF"; // Marker colours: NFDI4Earth light blue with white border
    const n4eDarkBlue = "#003F5F"; // Marker colours: NFDI4Earth dark blue with white border

    function createPersonalIcon(colour) {

        const markerHtmlStyle = `
        background-color: ${colour};
        display: block;
        width: 20px;
        height: 20px;
        border-radius: 20px 20px 0;
        transform: rotate(45deg);
        border: 1px solid #FFFFFF
        `

        return L.divIcon({
            className: "n4eIcon",
            iconAnchor: [10, 25],
            popupAnchor: [1.5, -20],
            html: `<span style='${markerHtmlStyle}'></span>`
        });

    };

    // Get logos that are missing on Wikidata or that are not scaled correctly from https://www.nfdi4earth.de/about-us/consortium
    // Use KH sourceSystemID (ROR ID or Wikidata ID)
    var missingLogos = [

        {
            // Alfred Wegener Institute for Polar and Marine Research
            sourceSystemID: "032e6b942",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/AWI_Logo_2017.png"
        },
        {
            // Bavarian State Archives
            sourceSystemID: "01hq2jk95",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/sab.png"
        },
        {
            // Bochum University of Applied Sciences
            sourceSystemID: "04x02q560",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/BO-Logo_m_Wortmarke_L.png"
        },
        {
            // Deutscher Wetterdienst
            sourceSystemID: "02nrqs528",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/DWD-Logo_2013.png"
        },
        {
            // Free University of Berlin
            sourceSystemID: "046ak2485",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/fuberlin_logo.png"
        },
        {
            // GEOMAR Helmholtz Centre for Ocean Research Kiel
            sourceSystemID: "02h2x0161",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/geomar.jpg"
        },
        {
            // Geo.X
            sourceSystemID: "http://www.wikidata.org/entity/Q115265089",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/GeoX_ausgeschnitten.png"
        },
        {
            // Geoverbund ABC/J
            sourceSystemID: "http://www.wikidata.org/entity/Q121462511",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/abcj_geoverbund-logo.png"
        },
        {
            // German Climate Computing Centre
            sourceSystemID: "03ztgj037",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/dkrz_logo.png"
        },
        {
            // German Marine Research Alliance
            sourceSystemID: "03rgygs91",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/dam.jpg"
        },
        {
            // Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
            sourceSystemID: "04z8jg394",
            logoURL: "	https://www.nfdi4earth.de/images/nfdi4earth/logos/GFZ_potsdam-CD_LogoRGB_DE.png"
        },
        {
            // Helmholtz-Zentrum Geesthacht Centre for Materials and Coastal Research
            sourceSystemID: "03qjp1d79",
            logoURL: "https://www.nfdi4earth.de/images/nicepage-images/hereon_Logo_standard_03_rgb.png"
        },
        {
            // Jülich Research Centre
            sourceSystemID: "02nv7yv05",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/FZj%C3%BClich_Logo.jpg"
        },
        {
            // Leibniz Centre for Agricultural Landscape Research
            sourceSystemID: "01ygyzs83",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/zalf.png"
        },
        {
            // Leibniz Institute for Applied Geophysics
            sourceSystemID: "05txczf44",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/liag.png"
        },
        {
            // Leibniz Institute for Tropospheric Research
            sourceSystemID: "03a5xsc56",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/tropos.jpg"
        },
        {
            // Leibniz Institute of Ecological Urban and Regional Development
            sourceSystemID: "02t26g637",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/i%C3%B6r.gif"
        },
        {
            // Leibniz Institute of Freshwater Ecology and Inland Fisheries
            sourceSystemID: "01nftxb06",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/IGB_farbe_pos.png"
        },
        {
            // Leipzig University
            sourceSystemID: "03s7gtk40",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/Universit%C3%A4t_Leipzig_logo.png"
        },
        {
            // Max Planck Institute for Biogeochemistry
            sourceSystemID: "051yxp643",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/BGC_LogoText_en_EPS.png"
        },
        {
            // Museum für Naturkunde - Leibniz Institute for Evolution and Biodiversity Science
            sourceSystemID: "052d1a351",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/mfn.jpg"
        },
        {
            // Potsdam Institute for Climate Impact Research
            sourceSystemID: "03e8s1d88",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/pik.png"
        },
        {
            // Research Institute for Sustainability
            sourceSystemID: "01vvnmw35",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/images/RIFSPotsdam.png"
        },
        {
            // Senckenberg Nature Research Society
            sourceSystemID: "00xmqmx64",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/Senckenberg-Logo.jpg"
        },
        {
            // Specialised Information Service for Geosciences
            sourceSystemID: "http://www.wikidata.org/entity/Q47491582",
            logoURL: "	https://www.nfdi4earth.de/images/nicepage-images/FID_GEO_Logo_rgb_randlos_max.jpg"
        },
        {
            // Technical University of Darmstadt
            sourceSystemID: "05n911h24",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/tu_darmstadt.png"
        },
        {
            // UNU-FLORES
            sourceSystemID: "03xp99m49",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/unu-flores.jpg"
        },
        {
            // University of Cologne
            sourceSystemID: "00rcxh774",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/unik%C3%B6ln.png"
        },
        {
            // University of Kiel
            sourceSystemID: "04v76ef78",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/unikiel.png"
        },
        {
            // University of Tübingen
            sourceSystemID: "03a1kwz48",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/unit%C3%BCbingen.png"
        },
        {
            // Water Science Alliance e.V
            sourceSystemID: "http://www.wikidata.org/entity/Q112117161",
            logoURL: "https://www.nfdi4earth.de/images/nfdi4earth/logos/wsaev.png"
        },

    ];

    // Query data from KH
    var query = [
        "PREFIX foaf: <http://xmlns.com/foaf/0.1/>",
        "PREFIX geo: <http://www.opengis.net/ont/geosparql#>",
        "PREFIX m4i: <http://w3id.org/nfdi4ing/metadata4ing#>",
        "PREFIX n4e: <http://nfdi4earth.de/ontology#>",
        "PREFIX sf: <http://www.opengis.net/ont/sf#>",
        "PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>",
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
        "PREFIX schema: <http://schema.org/>",
        "PREFIX org: <http://www.w3.org/ns/org#>",
        "CONSTRUCT {",
        "?organizationIRI rdfs:type foaf:Organization .",
        "?organizationIRI foaf:name ?institutionLabel .",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "?organizationIRI org:role ?roleWikidataIRI .",
        "?roleWikidataIRI rdfs:label ?role_label_en .",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "?organizationIRI vcard:country-name ?countryLabel .",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "?geom rdf:type sf:Point .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "WHERE {",
        "?nfdi4earthProject foaf:homepage <https://nfdi4earth.de/> .",
        "?organizationIRI org:hasMembership ?membership .",
        "?membership org:organization ?nfdi4earthProject .",
        "?membership org:role ?roleWikidataIRI .",
        "SERVICE <https://query.wikidata.org/sparql> {",
        "?roleWikidataIRI rdfs:label ?role_label_en",
        "FILTER(LANG(?role_label_en) = 'en') .",
        "}",
        "?organizationIRI foaf:name ?institutionLabel .",
        "OPTIONAL {",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "}",
        "OPTIONAL {",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:country-name ?countryLabel .}",
        "OPTIONAL {",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "}",
        "OPTIONAL {",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "}",
        "OPTIONAL {",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "}"
    ].join(" ");

    var queryLRZ = [
        "PREFIX foaf: <http://xmlns.com/foaf/0.1/>",
        "PREFIX geo: <http://www.opengis.net/ont/geosparql#>",
        "PREFIX m4i: <http://w3id.org/nfdi4ing/metadata4ing#>",
        "PREFIX n4e: <http://nfdi4earth.de/ontology#>",
        "PREFIX sf: <http://www.opengis.net/ont/sf#>",
        "PREFIX vcard: <http://www.w3.org/2006/vcard/ns#>",
        "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>",
        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>",
        "PREFIX schema: <http://schema.org/>",
        "PREFIX org: <http://www.w3.org/ns/org#>",

        "CONSTRUCT {",
        "?organizationIRI rdfs:type foaf:Organization .",
        "?organizationIRI foaf:name ?institutionLabel .",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "?organizationIRI vcard:country-name ?countryLabel .",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "?geom rdf:type sf:Point .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "WHERE {",
        "?parentOrganizationIRI m4i:hasRorId '001rdaz60' .",
        "?organizationIRI org:subOrganizationOf ?parentOrganizationIRI .",
        "?organizationIRI foaf:name ?institutionLabel .",
        "?organizationIRI foaf:homepage ?officialWebsite .",
        "OPTIONAL {",
        "?organizationIRI n4e:sourceSystemID ?sourceSystemID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI m4i:hasRorId ?rorID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:country-name ?countryLabel .}",
        "OPTIONAL {",
        "?organizationIRI vcard:locality ?headQuartersLocationLabel .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLocality ?headQuartersLocationGeonamesID .",
        "}",
        "OPTIONAL {",
        "?organizationIRI vcard:hasLogo ?logoURL .",
        "}",
        "OPTIONAL {",
        "?organizationIRI n4e:hasNFDI4EarthContactPerson ?contactPerson .",
        "}",
        "OPTIONAL {",
        "?organizationIRI geo:hasGeometry ?geom .",
        "?geom geo:asWKT ?coordinateLocation .",
        "}",
        "}"
    ].join(" ");

    var url = "https://nfdi4earth-knowledgehub.geo.tu-dresden.de/fuseki/knowledge-graph/sparql";

    $.ajax({
        method: "POST",
        dataType: "JSON",
        url: url,
        headers: { "accept": "application/ld+json" }, // Only accept JSON-LD as a response
        data: { query: query },
        success: function (graph) {

            console.log(graph);

            var promises = [];

            $.each(graph["@graph"], function (index, participant) {
                if (participant["type"] && participant["type"] === "foaf:Organization") { // Find organizations, ignore others
                    let promise = new Promise((resolve, reject) => {
                        // Exception for BADW --> Use information of suborganisation LRZ
                        if (participant["hasRorId"] == "001rdaz60") {

                            $.ajax({
                                method: "POST",
                                dataType: "JSON",
                                url: url,
                                headers: { "accept": "application/ld+json" }, // Only accept JSON-LD as a response
                                data: { query: queryLRZ },
                                success: function (LRZ) {

                                    graph["@graph"][index] = LRZ["@graph"][1]; // Replace BADW with LRZ
                                    graph["@graph"].push(LRZ["@graph"][0]); // Add geometry to array

                                    onEachParticipant(graph, index, resolve, reject);
                                }
                            });
                        }
                        else {
                            onEachParticipant(graph, index, resolve, reject);
                        };
                    });

                    promises.push(promise);
                };
            });

            var result = Promise.all(promises);
            result.then(function (data) {

                $("div.membersGrid > div").click(function () { // When card is clicked the corresponding popup is opened
                    markerOpenPopup($(this)[0].id);
                });

                // Put grid in alphabetical order
                var sortList = list => [...list].sort((a, b) => {
                    var A = a.textContent, B = b.textContent;
                    return (A < B) ? -1 : (A > B) ? 1 : 0;
                });

                const grid = $("#membersGrid")[0]; // Get unordered grid
                const gridElements = grid.querySelectorAll("div.member");
                grid.append(...sortList(gridElements));

            });

        }
    });

}); // Document ready